## A simple `echo` function for node.js to get started with GitLab Serverless.

Visit the [documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions) for more information.

## Using the example with Kubernetes secrets

If found, the example echo code will return a `MY_SECRET` secret variable stored as a Kubernetes secret.

The given secret should be created under the custom namespace used to deploy your functions. To find out the namespace, run `kubectl get namespaces`. The custom namespace is of the form `<project name>-<project id>-<environment>`. Once you have the namespace, you can create the secret using `kubectl create secret generic my-secrets -n <my namespace> --from-literal=MY_SECRET=imverysecure` under your custom namespace.